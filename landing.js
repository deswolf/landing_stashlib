var tl = new TimelineMax({onComplete:animateTheRest});
var cname = "animationComplete";

function animateLogo() {
  var wrapperEl = document.getElementById("wrapper");
  wrapperEl.removeAttribute("style");
  tl.to('#mark', 1, {x:220 , ease:Back.easeInOut})
  .from('#box', 2.5, {opacity:0, ease: Power2.easeOut})
  .from('#top_line', 1, {scaleX: 0, transformOrigin: "left", ease: Bounce.easeOut})
  .from('#bottom_line', 1, {scaleX: 0, transformOrigin: "right", ease: Bounce.easeOut})
  .to('#mark', 1, {x:0 , ease:Back.easeInOut})
  .from('#type', 3, {opacity:0, ease: Power4.easeOut});
}

function animateTheRest() {
  var mottoEl = document.getElementById("motto");
  var footerEl = document.getElementById("footer");
  mottoEl.className = cname;
  setTimeout(function() {
    footerEl.className = cname;
  }, 4000);
}

window.onload = animateLogo();